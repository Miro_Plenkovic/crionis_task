The program has two parts: a loop for entering the points in the set we will be observing in part 2, and a loop for selecting the points from that set

The first loop, there is an input to determine the action - 1 means entering a new point, 2 means continuing to the second part of the program, with the current set.

if 1 is selected, the next input asks for two integers separated by a comma (,) - x and y coordinates of the point that will be added. If the point is in the set already, it is ignored.

The second loop, there is again an input to determine the action - 1 means choosing a point, 2 means choosing a selection box,  and 3 ends the program.

if 1 is selected, the next input is again asking for two integers separated by a comma (,) - x and y coordinates of the point that is being selected. If the point is selected already, it gets deselected, and if it's not in the set, we empty the selected subset.

if 2 is selected, the next two inputs are asking for two integers separated by a comma (,) - the first one is the bottom left corner of the selection box, and the second one is the top right corner of the selection box. Then, all the points from the set that are inside the box, except the ones already selected, are added to the selected subset. If there are no points in the box, the selected subset is emptied.

The selected subset is sorted by the angle closed by the upper half of the y-axis, and the line that starts at the center of the coordinate system, and the point. That way, the points will be sorted clockwise, as per the requirement.