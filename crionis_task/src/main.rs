use std::f64::consts;
use std::io;
use std::str::FromStr;
use std::cmp::Ordering;

fn main() {
    let mut points:Vec<(i32, i32)> = vec![];
    let mut selected:Vec<(i32, i32)> = vec![];
    let mut _update:Option<Vec<(i32,i32)>>;

    loop {
        println!("points: {:?}", points);
        let mut choice = String::new();
        println!("Click 1 to add another point, or click 2 to continue");
        io::stdin().read_line(&mut choice).expect("error");
        let choice:i32 = choice.trim().parse().expect("Parsing error");
        match choice {
            1 => {
                let mut pointaccess = String::new();
                let mut _point = (0,0);
                println!("Enter the point as two integers separated by a comma (,)");
                io::stdin().read_line(&mut pointaccess).expect("error");
                let it0 = pointaccess.split(',');
                if it0.clone().count() == 2 {
                    let mut it = it0.map(|s| i32::from_str(s.trim()).expect("failed to parse number"));
                    _point =
                        (it.next().unwrap(), it.next().unwrap());
                    if points.iter().position(|&x| x == _point) == None
                    {
                        points.push(_point);
                    }
                }
                else {
                    println!("Parsing error!");
                }
            },
            2=> {
                break;
            }
            _ => {
                println!("invalid selection!");
            }
        }
    }

    loop{
        println!("selected points: {:?}", selected);
        let mut method = String::new();
        println!("Enter the selection method: (1: point, 2: box, 3: exit)");
        io::stdin().read_line(&mut method).expect("error");
        let method:i32 = method.trim().parse().expect("Parsing error");
        let mut point1access = String::new();
        let point1;
        let mut point2access = String::new();
        let point2;

        match method {
            1 => {
                println!("Enter the point as two integers separated by a comma (,)");
                io::stdin().read_line(&mut point1access).expect("error");
                let it0 = point1access.split(',');
                if it0.clone().count() == 2 {
                    let mut it = it0.map(|s| i32::from_str(s.trim()).expect("failed to parse number"));
                    point1 =
                        (it.next().unwrap(), it.next().unwrap());
                }
                else {
                    println!("Parsing error!");
                    continue;
                }
                let update = click_point(&mut points, point1);
                
                match update {
                None => {
                    selected.clear();
                }
                Some(val) => {
                    match selected.iter().position(|&x| x == val[0]){
                        Some(_index) => {
                            selected.remove(_index);
                        }
                        None => {
                            selected.push(val[0]);
                        }
                    }
                    }
                }
            }
            2 => {
                println!("Enter the bottom-left point of the box as two integers separated by a comma (,)");
                io::stdin().read_line(&mut point1access).expect("error");
                let it0 = point1access.split(',');
                if it0.clone().count() == 2 {
                    let mut it = it0.map(|s| i32::from_str(s.trim()).expect("failed to parse number"));
                    point1 =
                        (it.next().unwrap(), it.next().unwrap());
                }
                else {
                    println!("Parsing error!");
                    continue;
                }
                println!("Enter the top-right point of the box as two integers separated by a comma (,)");
                io::stdin().read_line(&mut point2access).expect("error");
                let it0 = point2access.split(',');
                if it0.clone().count() == 2 {
                    let mut it = it0.map(|s| i32::from_str(s.trim()).expect("failed to parse number"));
                    point2 =
                        (it.next().unwrap(), it.next().unwrap());
                }
                else {
                    println!("Parsing error!");
                    continue;
                }
                let update = select_box(&mut points, point1, point2);
                if update.len() == 0 {
                    selected.clear();
                }
                else {
                    for (xi,yi) in update.iter(){
                        if selected.iter().position(|&x| x == (*xi,*yi)) == None
                        {
                            selected.push((*xi,*yi));
                        }
                    }
                }
            }
            3 => {
                break;
            }
            _ => {
                println!("invalid selection!");
            }
        }
        selected.sort_by(|(xa,ya),(xb,yb)| match angle((*xa,*ya)) < (angle((*xb,*yb))) {true => Ordering::Less, false => Ordering::Greater})
    }
}

fn angle((x,y):(i32,i32)) -> f64{
    if x >= 0 {
        return (y as f64/((x.pow(2)+y.pow(2)) as f64).sqrt()).acos()
    }
    else {
        -(y as f64/((x.pow(2)+y.pow(2)) as f64).sqrt()).acos() + (2 as f64)*consts::PI
    }
}

fn click_point(v: &mut Vec<(i32, i32)>, (x,y): (i32, i32)) -> Option<Vec<(i32,i32)>>{
    if (*v).contains(&(x,y)) {
        return Some(vec![(x,y)]);
    }
    else {
        None
    }
}

fn select_box(v: &mut Vec<(i32, i32)>, (x1,y1): (i32, i32), (x2,y2): (i32, i32)) -> Vec<(i32,i32)>{
    let mut resp:Vec<(i32,i32)> = vec![];
    for (xi,yi) in v.iter(){
        if x1<=*xi && x2>=*xi && y1<=*yi && y2>=*yi {
            resp.push((*xi,*yi));
        } 
    }
    resp
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn angle_check() {
        assert_eq!(angle((1,1)), 0.7853981633974484);
    }
    #[test]
    fn click_point_check() {
        assert_eq!(click_point(&mut vec![(1,1), (-1,-1)], (1,1)), Some(vec![(1,1)]));
    }
    #[test]
    fn select_box_check() {
        assert_eq!(select_box(&mut vec![(1,1), (-1,-1)], (0,0), (2,2)), vec![(1,1)]);
    }
}
